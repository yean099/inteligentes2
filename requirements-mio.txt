AMP==1.1.4
apriori==1.0.0
caldav==0.1.4
coverage==4.5.4
cycler==0.10.0
Django==2.2.3
django-basic-stats==0.2.0
efficient-apriori==1.0.0
inline==0.0.1
joblib==0.13.2
kiwisolver==1.1.0
lxml==4.4.0
matplotlib==3.1.1
mlxtend==0.17.0
nose==1.3.7
numpy==1.17.0
pandas==0.25.0
pyfpgrowth==1.0
pyparsing==2.4.1.1
python-dateutil==2.8.0
pytz==2019.1
scikit-learn==0.21.2
scipy==1.3.0
seaborn==0.9.0
six==1.12.0
sqlparse==0.3.0
vobject==0.9.6.1

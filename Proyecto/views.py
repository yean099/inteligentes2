import numpy as np 
import tensorflow as tf 
import matplotlib.pyplot as plt
from django.http import HttpResponse,HttpResponseRedirect
from django.shortcuts import get_object_or_404 ,render
from django.urls import reverse
from .models import Question,Choice
from io import BytesIO
import base64
import io
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import pyfpgrowth
from efficient_apriori import apriori
from sklearn import datasets
from sklearn import metrics,svm
from sklearn.naive_bayes import GaussianNB
from sklearn import neighbors, datasets
import pandas as pd
from sklearn.cluster import KMeans
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from django.core.files import File


def linear(request):
    out=""
    x = [1,2,3,4]
    y = [3,5,7,10]

    fit = np.polyfit(x,y,1)
    fit_fn = np.poly1d(fit)
    f = plt.figure()
    plt.title('Linear Regression Result')
    plt.plot(x,y,'green', label ='Original data') 
    plt.plot(x,y, 'yo', x, fit_fn(x), '--k')
    plt.legend()
    plt.xlim([0, max(x)+1])
    plt.ylim([0, max(y)+1])

    out+="función que toma x y devuelve una estimación para y ="+str(fit_fn(x))

    buffer = BytesIO()
    plt.savefig(buffer, format='png')
    buffer.seek(0)
    image_png = buffer.getvalue()
    buffer.close()

    graphic = base64.b64encode(image_png)
    graphic = graphic.decode('utf-8')

    data = {
        'name': 'linear regression',
        'graphic': graphic,
        'out':out,
        'exp': 'En estadística la regresión lineal o ajuste lineal es un modelo matemático usado para aproximar la relación de dependencia entre una variable dependiente Y, las variables independientes Xi y un término aleatorio ε.'
    }
    return render(request, 'Proyecto/Linear.html', {'data': data})


def kmeans(request):
    out = ""
    x1 = np.array([3, 1, 1, 2, 1, 6, 6, 6, 5, 6, 7, 8, 9, 8, 9, 9, 8])
    x2 = np.array([5, 4, 6, 6, 5, 8, 6, 7, 6, 7, 1, 2, 1, 2, 3, 2, 3])

    plt.xlim([0, max(x1)+1])
    plt.ylim([0, max(x2)+1])
    plt.title('Dataset para K-means')
    plt.scatter(x1, x2)
    
    X = np.array(list(zip(x1, x2))).reshape(len(x1), 2)
    colors = ['y', 'r', 'b']
    markers = ['o', 'v', 's']

    # KMeans algorithm 
    K = 3
    kmeans_model = KMeans(n_clusters=K).fit(X)

    for i, l in enumerate(kmeans_model.labels_):
        plt.plot(x1[i], x2[i], color=colors[l], marker=markers[l],ls='None')
        plt.xlim([0, max(x1)+1])
        plt.ylim([0, max(x2)+1])

    buffer = BytesIO()
    plt.savefig(buffer, format='png')
    buffer.seek(0)
    image_png = buffer.getvalue()
    buffer.close()

    graphic = base64.b64encode(image_png)
    graphic = graphic.decode('utf-8')

    data = {
        'name': 'K-means',
        'graphic': graphic,
        'out': out,
        'exp': 'K-means es un método de agrupamiento, que tiene como objetivo la partición de un conjunto de n observaciones en k grupos en el que cada observación pertenece al grupo cuyo valor medio es más cercano. Es un método utilizado en minería de datos.'
    }
    return render(request, 'Proyecto/kmeans.html', {'data': data})

def fpgrouth(request):
    
    transitions = [('aceite', 'leche', 'azucar'),
                    ('aceite', 'yerba', 'cerveza','azucar'),
                    ('leche', 'yerba', 'cerveza','sal'),
                    ('aceite','leche','yerba','sal')]

    patterns = pyfpgrowth.find_frequent_patterns(transitions,2)
    rules = pyfpgrowth.generate_association_rules(patterns, 0.7)

    data={
        'name':'fpgrouth',
        'patterns': patterns,
        'rules' :rules
    }

    return render(request, 'Proyecto/fpgrouth.html',{'data':data})

def apriori_algoritm(request):
    transactions = [('a', 'b', 'c','d'),
                ('a', 'b', 'd'),
                ('a', 'b'),
                ('b','c','d'),
                ('b','c'),
                ('c','d'),
                ('b','d')]
    itemsets, rules = apriori(transactions, min_support=0.4,  min_confidence=0.7)

    i=(len(itemsets))
    cont=0
    cadena2=""
    cadena1=""

    #se recorren las iteraciones K que se realizan
    for x in range(1,i+1):
        
        cadena1+="K=" + str(x) +" "
        cadena1+=str(itemsets[x])+"\n"
 

    # Print out every rule with 2 items on the left hand side,
    # 1 item on the right hand side, sorted by lift
    rules_rhs = filter(lambda rule: len(rule.lhs) == 2 and len(rule.rhs) == 1, rules)

    

    for rule in sorted(rules_rhs, key=lambda rule: rule.lift):
        print(rule)
       # cadena2+= str(rule) +"\n" # Prints the rule and its confidence, support, lift, ...

    data={
        'name':'apriori',
        'items': cadena1,
        'rules' :rules
    }

    #print(cadena1,cadena2)
    return render(request, 'Proyecto/apriori.html',{'data':data})


def bayes(request):
   

    dataset = datasets.load_iris()

    d=([[5.1, 3.5, 1.4, 0.2],
        [4.9, 3. , 1.4, 0.2],
        [4.7, 3.2, 1.3, 0.2],
        [4.6, 3.1, 1.5, 0.2],
        [5. , 3.6, 1.4, 0.2],
        [5.4, 3.9, 1.7, 0.4],
        [4.6, 3.4, 1.4, 0.3],
        [5. , 3.4, 1.5, 0.2],
        [4.4, 2.9, 1.4, 0.2],
        [4.9, 3.1, 1.5, 0.1],
        [5.4, 3.7, 1.5, 0.2],
        [4.8, 3.4, 1.6, 0.2],
        [4.8, 3. , 1.4, 0.1],
        [4.3, 3. , 1.1, 0.1],
        [5.8, 4. , 1.2, 0.2],
        [5.7, 4.4, 1.5, 0.4],
        [5.4, 3.9, 1.3, 0.4],
        [5.1, 3.5, 1.4, 0.3],
        [5.7, 3.8, 1.7, 0.3],
        [5.1, 3.8, 1.5, 0.3],
        [5.4, 3.4, 1.7, 0.2],
        [5.1, 3.7, 1.5, 0.4],
        [4.6, 3.6, 1. , 0.2],
        [5.1, 3.3, 1.7, 0.5],
        [4.8, 3.4, 1.9, 0.2],
        [5. , 3. , 1.6, 0.2],
        [5. , 3.4, 1.6, 0.4],
        [5.2, 3.5, 1.5, 0.2],
        [5.2, 3.4, 1.4, 0.2],
        [4.7, 3.2, 1.6, 0.2],
        [4.8, 3.1, 1.6, 0.2],
        [5.4, 3.4, 1.5, 0.4],
        [5.2, 4.1, 1.5, 0.1],
        [5.5, 4.2, 1.4, 0.2],
        [4.9, 3.1, 1.5, 0.2],
        [5. , 3.2, 1.2, 0.2],
        [5.5, 3.5, 1.3, 0.2],
        [4.9, 3.6, 1.4, 0.1],
        [4.4, 3. , 1.3, 0.2],
        [5.1, 3.4, 1.5, 0.2],
        [5. , 3.5, 1.3, 0.3],
        [4.5, 2.3, 1.3, 0.3],
        [4.4, 3.2, 1.3, 0.2],
        [5. , 3.5, 1.6, 0.6],
        [5.1, 3.8, 1.9, 0.4],
        [4.8, 3. , 1.4, 0.3],
        [5.1, 3.8, 1.6, 0.2],
        [4.6, 3.2, 1.4, 0.2],
        [5.3, 3.7, 1.5, 0.2],
        [5. , 3.3, 1.4, 0.2],
        [7. , 3.2, 4.7, 1.4],
        [6.4, 3.2, 4.5, 1.5],
        [6.9, 3.1, 4.9, 1.5],
        [5.5, 2.3, 4. , 1.3],
        [6.5, 2.8, 4.6, 1.5],
        [5.7, 2.8, 4.5, 1.3],
        [6.3, 3.3, 4.7, 1.6],
        [4.9, 2.4, 3.3, 1. ],
        [6.6, 2.9, 4.6, 1.3],
        [5.2, 2.7, 3.9, 1.4],
        [5. , 2. , 3.5, 1. ],
        [5.9, 3. , 4.2, 1.5],
        [6. , 2.2, 4. , 1. ],
        [6.1, 2.9, 4.7, 1.4],
        [5.6, 2.9, 3.6, 1.3],
        [6.7, 3.1, 4.4, 1.4],
        [5.6, 3. , 4.5, 1.5],
        [5.8, 2.7, 4.1, 1. ],
        [6.2, 2.2, 4.5, 1.5],
        [5.6, 2.5, 3.9, 1.1],
        [5.9, 3.2, 4.8, 1.8],
        [6.1, 2.8, 4. , 1.3],
        [6.3, 2.5, 4.9, 1.5],
        [6.1, 2.8, 4.7, 1.2],
        [6.4, 2.9, 4.3, 1.3],
        [6.6, 3. , 4.4, 1.4],
        [6.8, 2.8, 4.8, 1.4],
        [6.7, 3. , 5. , 1.7],
        [6. , 2.9, 4.5, 1.5],
        [5.7, 2.6, 3.5, 1. ],
        [5.5, 2.4, 3.8, 1.1],
        [5.5, 2.4, 3.7, 1. ],
        [5.8, 2.7, 3.9, 1.2],
        [6. , 2.7, 5.1, 1.6],
        [5.4, 3. , 4.5, 1.5],
        [6. , 3.4, 4.5, 1.6],
        [6.7, 3.1, 4.7, 1.5],
        [6.3, 2.3, 4.4, 1.3],
        [5.6, 3. , 4.1, 1.3],
        [5.5, 2.5, 4. , 1.3],
        [5.5, 2.6, 4.4, 1.2],
        [6.1, 3. , 4.6, 1.4],
        [5.8, 2.6, 4. , 1.2],
        [5. , 2.3, 3.3, 1. ],
        [5.6, 2.7, 4.2, 1.3],
        [5.7, 3. , 4.2, 1.2],
        [5.7, 2.9, 4.2, 1.3],
        [6.2, 2.9, 4.3, 1.3],
        [5.1, 2.5, 3. , 1.1],
        [5.7, 2.8, 4.1, 1.3],
        [6.3, 3.3, 6. , 2.5],
        [5.8, 2.7, 5.1, 1.9],
        [7.1, 3. , 5.9, 2.1],
        [6.3, 2.9, 5.6, 1.8],
        [6.5, 3. , 5.8, 2.2],
        [7.6, 3. , 6.6, 2.1],
        [4.9, 2.5, 4.5, 1.7],
        [7.3, 2.9, 6.3, 1.8],
        [6.7, 2.5, 5.8, 1.8],
        [7.2, 3.6, 6.1, 2.5],
        [6.5, 3.2, 5.1, 2. ],
        [6.4, 2.7, 5.3, 1.9],
        [6.8, 3. , 5.5, 2.1],
        [5.7, 2.5, 5. , 2. ],
        [5.8, 2.8, 5.1, 2.4],
        [6.4, 3.2, 5.3, 2.3],
        [6.5, 3. , 5.5, 1.8],
        [7.7, 3.8, 6.7, 2.2],
        [7.7, 2.6, 6.9, 2.3],
        [6. , 2.2, 5. , 1.5],
        [6.9, 3.2, 5.7, 2.3],
        [5.6, 2.8, 4.9, 2. ],
        [7.7, 2.8, 6.7, 2. ],
        [6.3, 2.7, 4.9, 1.8],
        [6.7, 3.3, 5.7, 2.1],
        [7.2, 3.2, 6. , 1.8],
        [6.2, 2.8, 4.8, 1.8],
        [6.1, 3. , 4.9, 1.8],
        [6.4, 2.8, 5.6, 2.1],
        [7.2, 3. , 5.8, 1.6],
        [7.4, 2.8, 6.1, 1.9],
        [7.9, 3.8, 6.4, 2. ],
        [6.4, 2.8, 5.6, 2.2],
        [6.3, 2.8, 5.1, 1.5],
        [6.1, 2.6, 5.6, 1.4],
        [7.7, 3. , 6.1, 2.3],
        [6.3, 3.4, 5.6, 2.4],
        [6.4, 3.1, 5.5, 1.8],
        [6. , 3. , 4.8, 1.8],
        [6.9, 3.1, 5.4, 2.1],
        [6.7, 3.1, 5.6, 2.4],
        [6.9, 3.1, 5.1, 2.3],
        [5.8, 2.7, 5.1, 1.9],
        [6.8, 3.2, 5.9, 2.3],
        [6.7, 3.3, 5.7, 2.5],
        [6.7, 3. , 5.2, 2.3],
        [6.3, 2.5, 5. , 1.9],
        [6.5, 3. , 5.2, 2. ],
        [6.2, 3.4, 5.4, 2.3],
        [5.9, 3. , 5.1, 1.8]])

    t=([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2])

    #print(dataset)
    #print(dataset.data)
    #print(dataset.target)

    cadena1=""
    cadena2=""
    model = GaussianNB()
    model.fit(d,t)


    expected = dataset.target
    predicted = model.predict(dataset.data)
   

    cadena1=str(metrics.classification_report(expected, predicted))
    cadena2=str(metrics.confusion_matrix(expected, predicted))
    print(cadena1,cadena2)

    data={
        'name':'Bayes',
        'items': cadena1,
        'rules' :cadena2
    }

    return render(request, 'Proyecto/bayes.html',{'data':data})

def knn(request):
    n_neighbors = 12

    # import some data to play with
    iris = datasets.load_iris()

    # prepare data
    X = iris.data[:, :2]
    y = iris.target
    h = .02

    # Create color maps
    cmap_light = ListedColormap(['#FFAAAA', '#AAFFAA','#00AAFF'])
    cmap_bold = ListedColormap(['#FF0000', '#00FF00','#00AAFF'])

    # we create an instance of Neighbours Classifier and fit the data.
    clf = neighbors.KNeighborsClassifier(n_neighbors, weights='distance')
    clf.fit(X, y)

    # Los datos que se deben ingresar para clasificar
    sl = 10
    sw = 10
    clase=""
    dataClass = clf.predict([[sl,sw]])
    print('Prediction: '),
    
    if dataClass == 0:
        clase="clase 0"
        print('clase 0')
    elif dataClass == 1:
        clase="clase 1"
        print('Clase 1')
    else:
        clase="clase 2"
        print('Clase 2')

    # calculate min, max and limits
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
    np.arange(y_min, y_max, h))

    # predict class using data and kNN classifier
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    
    
   

    f = plt.figure()
    axes = f.add_axes([0.15, 0.15, 0.75, 0.75]) # [left, bottom, width, height]
    axes.pcolormesh(xx, yy, Z, cmap=cmap_light)

    # Plot also the training points
    axes.scatter(X[:, 0], X[:, 1], c=y, cmap=cmap_bold)
    axes.set_title("3-Class classification (k = %i)" % (n_neighbors))

    buffer = BytesIO()
    plt.savefig(buffer, format='png')
    buffer.seek(0)
    image_png = buffer.getvalue()
    buffer.close()

    graphic = base64.b64encode(image_png)
    graphic = graphic.decode('utf-8')

    data={
        'name':'Knn',
        'graphic':graphic,
        'out':clase,
        'exp':'En estadística la regresión lineal o ajuste lineal es un modelo matemático usado para aproximar la relación de dependencia entre una variable dependiente Y, las variables independientes Xi y un término aleatorio ε.'
    }
    return render(request, 'Proyecto/knn.html', {'data':data})


def s_vm(request):
        #iris = pd.read_csv('../Datasets/credit_card.csv')
    iris= datasets.load_iris()

    X = iris.data[:, :2]  # Solo tomamos las dos primeras características.
    y = iris.target

    h = .02  # tamaño de paso en la malla

    # creamos una instancia de SVM y equipamos datos. No escalamos nuestros
    # datos ya que queremos trazar los vectores de soporte
    C = 1.0  # SVM regularizamos los parametros

    kernel="rbf"
    i = -1
    if kernel =="linear":
        clf = svm.SVC(kernel='linear', C=C).fit(X, y)
        i=0
    if kernel =="rbf":
        clf = svm.SVC(kernel='rbf', gamma=0.7, C=C).fit(X, y)
        i=1
    if kernel == "poly":
        clf = svm.SVC(kernel='poly', degree=3, C=C).fit(X, y)
        i=2

    
    # crear una malla para trazar
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
    np.arange(y_min, y_max, h))

    # los titulos del plano
    titles = ['SVM con kernel lineal',
    'SMV con kernel RBF',
    'SMV con  kernel polynomial']

    #for i, clf in enumerate((svc, lin_svc, rbf_svc, poly_svc)):

    # Trace el límite de decisión. Para eso, asignaremos un color a cada
    # punto en la malla [x_min, x_max] x [y_min, y_max].

    plt.subplots_adjust(wspace=0.4, hspace=0.4)
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    # introducimos los resultados en cada punto de color
    Z = Z.reshape(xx.shape)
    


    # Trazar también los puntos de entrenamiento
    f = plt.figure()
    plt.contourf(xx, yy, Z, cmap=plt.cm.coolwarm, alpha=0.8)
    plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.coolwarm)
    plt.xlabel('Sepal length')
    plt.ylabel('Sepal width')
    plt.xlim(xx.min(), xx.max())
    plt.ylim(yy.min(), yy.max())
    plt.xticks(())
    plt.yticks(())
    plt.title(titles[i])
    plt.plot()

    buffer = BytesIO()
    plt.savefig(buffer, format='png')
    buffer.seek(0)
    image_png = buffer.getvalue()
    buffer.close()

    graphic = base64.b64encode(image_png)
    graphic = graphic.decode('utf-8')

    data={
        'name':'SVM',
        'graphic':graphic,
        'out':'holi',
        'exp':'En estadística la regresión lineal o ajuste lineal es un modelo matemático usado para aproximar la relación de dependencia entre una variable dependiente Y, las variables independientes Xi y un término aleatorio ε.'
    }
    return render(request, 'Proyecto/knn.html', {'data':data})


def index(request):
    if request.method == 'POST':
        try:
            csv_file = request.FILES['myfile']
            df=pd.read_csv(csv_file, delimiter = ',')
            a=str(df)
            print(a)
        except:
            print("Un error ha ocurrido")
        return render(request, 'Proyecto/index.html')
    else:
        return render(request, 'Proyecto/index.html')










from django.urls import path
from . import views

app_name = 'Proyecto'
urlpatterns = [
    path('', views.index, name='index'),
    path('linear', views.linear, name='linear'),
    path('kmeans', views.kmeans, name='kmeans'),
    path('fpgrouth', views.fpgrouth, name='fpgrouth'),
    path('apriori', views.apriori_algoritm, name='apriori'),
    path('bayes', views.bayes, name='bayes'),
    path('knn', views.knn, name='knn'),
    path('svm', views.s_vm, name='svm'),

]